#!/bin/bash

CWD=`pwd`
TAGGING_FILE=tagfiles.list
LIST_FILE=cscope.files
DATABASE_FILE=cscope.out
PATH="/usr/local/bin:/sbin:/usr/sbin:/bin:/usr/bin:$PATH"

INDEX_DIR () {
	echo "search directory : $1"
	find ./$1 \( -type f -o -type l \) | \
		egrep -i '\.([chly](xx|pp)*|cc|hh|java|mk|pl|sh|xml|s|S|csv|py)$' | \
		sed -e '/\/CVS\//d' -e '/\/test\//d' -e '/\/RCS\//d' -e 's/^\.\///' \
		>> ${CWD}/${TAGGING_FILE}
}

EXCLUDE () {
	mv ${CWD}/${TAGGING_FILE} ${CWD}/.${TAGGING_FILE}.temp
	cat ${CWD}/.${TAGGING_FILE}.temp | \
		sed -e $1 \
		>> ${CWD}/${TAGGING_FILE}
	rm -f ${CWD}/.${TAGGING_FILE}.temp
}

echo "Clean up old files ..."
rm -f ${CWD}/${TAGGING_FILE}
rm -f ${CWD}/.${TAGGING_FILE}.temp
rm -f ${CWD}/${LIST_FILE}
rm -f ${CWD}/${DATABASE_FILE}
rm -f ${CWD}/tags

echo "Creating list of files to index ..."
INDEX_DIR ./micro_mouse

echo "Remove some index ..."
EXCLUDE '/\/\<out\>\//d'

# sort
echo "Creating list of files to index ..."
cat ${CWD}/${TAGGING_FILE} | sort > ${CWD}/${LIST_FILE}
rm -f ${CWD}/${TAGGING_FILE}

echo "generate cscope database ..."
cscope -b -i ${LIST_FILE} -f ${DATABASE_FILE} 2>/dev/null

echo "generate ctags database ..."
ctags -R --c++-kinds=+p --fields=+iaSl --extra=+q -L ${LIST_FILE} 2>/dev/null

#echo "generate gtags database ..."
#gtags -f ${LIST_FILE} 2>/dev/null

exit 0
