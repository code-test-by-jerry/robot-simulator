/* Copyright 2022. Lee, Jerry J all rights reserved */

#include <data/robot_information.h>

RobotInformation::RobotInformation()
    : utime(-1), speed(-1), enter(-1), accel(-1), const_vel(-1), escape(-1),
      tread(-1), left_acc(-1), right_acc(-1) {}

void RobotInformation::SetUtime(int utime) { this->utime = utime; }
void RobotInformation::SetSpeed(int speed) { this->speed = speed; }
void RobotInformation::SetEnter(int enter) { this->enter = enter; }
void RobotInformation::SetAccel(int accel) { this->accel = accel; }
void RobotInformation::SetConstVel(int const_vel) {
  this->const_vel = const_vel;
}
void RobotInformation::SetEscape(int escape) { this->escape = escape; }
void RobotInformation::SetTread(int tread) { this->tread = tread; }
void RobotInformation::SetLeftAcc(int left_acc) { this->left_acc = left_acc; }
void RobotInformation::SetRightAcc(int right_acc) {
  this->right_acc = right_acc;
}
void RobotInformation::SetLeftVel(double left_vel) {
  this->left_vel = left_vel;
}
void RobotInformation::SetRightVel(double right_vel) {
  this->right_vel = right_vel;
}

int RobotInformation::GetUtime(void) const { return this->utime; }
int RobotInformation::GetSpeed(void) const { return this->speed; }
int RobotInformation::GetEnter(void) const { return this->enter; }
int RobotInformation::GetAccel(void) const { return this->accel; }
int RobotInformation::GetConstVel(void) const { return this->const_vel; }
int RobotInformation::GetEscape(void) const { return this->escape; }
int RobotInformation::GetTread(void) const { return this->tread; }
int RobotInformation::GetLeftAcc(void) const { return this->left_acc; }
int RobotInformation::GetRightAcc(void) const { return this->right_acc; }
double RobotInformation::GetLeftVel(void) const { return this->left_vel; }
double RobotInformation::GetRightVel(void) const { return this->right_vel; }
