/* Copyright 2022. Lee, Jerry J all rights reserved */

#include <data/common_compute.h>

#include <cmath>

#define MICRO_SEC 1000000.0

/* 180 / Pie */
#define RADIAN_TO_DEGREE 57.295791433

/* Pi / 180 */
#define DEGREE_TO_RADIAN 0.017453289

static inline double _compute_next_dist_non_acc(int utime, double vel);
static inline double _compute_next_vel(double cur_vel, double cur_acc,
                                       double utime);
static inline double _compute_next_dist(double cur_vel, double next_vel,
                                        double cur_acc);
static inline double _compute_center_cordi(double ratio_m, double ratio_n,
                                           double cordi_a, double cordi_b);
static inline std::pair<double, double> _compute_next_cordi(double base_x,
                                                            double base_y,
                                                            double x, double y,
                                                            double theta);
static inline QRectF _set_rect(double x, double y, double radius);
static inline int _set_start_angle(double cx, double cy, double x, double y);
static inline int _set_straight_cordi(double *x1, double *y1, double *x2,
                                      double *y2, double dist);

CommonCompute::CommonCompute()
    : cur_left_x(0.0), cur_right_x(0.0), cur_left_y(0.0), cur_right_y(0.0),
      left_cordi(), right_cordi(), span_angle(0) {}

CommonCompute &CommonCompute::operator=(const CommonCompute &cc) {
  this->cur_left_x = cc.cur_left_x;
  this->cur_right_x = cc.cur_right_x;
  this->cur_left_y = cc.cur_left_y;
  this->cur_right_y = cc.cur_right_y;

  return *this;
}

void CommonCompute::SetStraightMode(const int robot_tread) {
  double tread = static_cast<double>(robot_tread);
  double def = static_cast<double>(Coordinates::kStraightDef);

  /* set the start coordinates */
  cur_left_x = def - tread;
  cur_right_x = def + tread;
  cur_left_y = cur_right_y = def;
}

void CommonCompute::SetDiagonalMode(const int robot_tread) {
  double tread = static_cast<double>(robot_tread);

  /* sqrt(x^2 + y^2) = tread, x == y */
  double move_cordi = sqrt(tread * tread / 2.0);
  double x_ref = static_cast<double>(Coordinates::kDiagonalXDef);
  double y_ref = static_cast<double>(Coordinates::kDiagonalYDef);

  /* set the start coordinates */
  cur_left_x = x_ref - move_cordi;
  cur_left_y = y_ref + move_cordi;
  cur_right_x = x_ref + move_cordi;
  cur_right_y = y_ref - move_cordi;
}

double CommonCompute::GetCurLeftX(void) const { return cur_left_x; }
double CommonCompute::GetCurRightX(void) const { return cur_right_x; }
double CommonCompute::GetCurLeftY(void) const { return cur_left_y; }
double CommonCompute::GetCurRightY(void) const { return cur_right_y; }
QRectF CommonCompute::GetLeftCordi(void) const { return left_cordi; }
QRectF CommonCompute::GetRightCordi(void) const { return right_cordi; }
int CommonCompute::GetStartAngle(void) const { return start_angle; }
int CommonCompute::GetSpanAngle(void) const { return span_angle; }

void CommonCompute::EnterOrEscape(const int utime, const int speed) {
  /* compute next dist */
  double next_dist =
      _compute_next_dist_non_acc(utime, static_cast<double>(speed));

  /* set next coordinates */
  _set_straight_cordi(&cur_left_x, &cur_left_y, &cur_right_x, &cur_right_y,
                      next_dist);
}

std::pair<double, double>
CommonCompute::Accel(const int utime, const double left_vel,
                     const double right_vel, const int left_acc,
                     const int right_acc, const int robot_tread,
                     const int ref_cordi) {
  if (left_acc == 0 || right_acc == 0)
    return std::make_pair(-1.0, -1.0);

  double u_time = static_cast<double>(utime);
  double l_acc = static_cast<double>(left_acc);
  double r_acc = static_cast<double>(right_acc);
  double tread = static_cast<double>(robot_tread);
  double base = static_cast<double>(ref_cordi);

  /* V = Vot + at */
  double next_left_vel = _compute_next_vel(left_vel, -l_acc, u_time);
  double next_right_vel = _compute_next_vel(right_vel, r_acc, u_time);

  if (next_left_vel < 0.0 || next_right_vel < 0.0)
    return std::make_pair(-1.0, -1.0);

  /* |V^2 - Vo^2| = 2as */
  double next_left_dist =
      _compute_next_dist(next_left_vel, left_vel, fabs(l_acc));
  double next_right_dist =
      _compute_next_dist(next_right_vel, right_vel, fabs(r_acc));

  /* compute all */
  compute_moving_data(next_left_dist, next_right_dist, tread, base);

  return std::make_pair(static_cast<double>(next_left_vel),
                        static_cast<double>(next_right_vel));
}

int CommonCompute::ConstVel(const int utime, const double left_vel,
                            const double right_vel, const int robot_tread,
                            const int ref_cordi) {
  double tread = static_cast<double>(robot_tread);
  double base = static_cast<double>(ref_cordi);

  /* S = Vot */
  double next_left_dist = _compute_next_dist_non_acc(utime, left_vel);
  double next_right_dist = _compute_next_dist_non_acc(utime, right_vel);

  /* compute all */
  compute_moving_data(next_left_dist, next_right_dist, tread, base);

  return 0;
}

void CommonCompute::compute_moving_data(double next_left_dist,
                                        double next_right_dist, double tread,
                                        double base) {
  /* Double the coordinates for the drawing */
  tread *= 2.0;
  next_left_dist *= 2.0;
  next_right_dist *= 2.0;

  /* l = r * theta */
  double theta = (next_right_dist - next_left_dist) / tread;
  double radius = next_left_dist / theta;

  /* compute the center of circle using the point of external division */
  double center_x =
      _compute_center_cordi(radius, radius + tread, cur_left_x, cur_right_x);
  double center_y =
      _compute_center_cordi(radius, radius + tread, cur_left_y, cur_right_y);

  /* compute the start angle */
  start_angle = _set_start_angle(center_x, center_y, cur_left_x, cur_left_y);

  /* next cordinate from center of circle */
  std::pair<double, double> cordi;
  cordi =
      _compute_next_cordi(center_x, center_y, cur_left_x, cur_left_y, theta);
  cur_left_x = cordi.first;
  cur_left_y = cordi.second;

  cordi =
      _compute_next_cordi(center_x, center_y, cur_right_x, cur_right_y, theta);
  cur_right_x = cordi.first;
  cur_right_y = cordi.second;

  /* set the reference coordinates and angle for drawing */
  left_cordi = _set_rect(base + center_x, base + center_y, radius);
  right_cordi = _set_rect(base + center_x, base + center_y, radius + tread);

  /* radian * 180/Pi = degree and for drawArc, multiple 16 */
  span_angle = static_cast<int>(theta * RADIAN_TO_DEGREE * 16.0 + 0.5);
}

static inline double _compute_next_dist_non_acc(int utime, double vel) {
  /* S = Vot */
  return static_cast<double>(utime) * vel / MICRO_SEC;
}

static inline double _compute_next_vel(double cur_vel, double cur_acc,
                                       double utime) {
  /* V = Vot + at */
  return cur_vel + (cur_acc * utime / MICRO_SEC);
}

static inline double _compute_next_dist(double cur_vel, double next_vel,
                                        double cur_acc) {
  /* |V^2 - Vo^2| = 2as */
  return fabs(pow(next_vel, 2) - pow(cur_vel, 2)) / (2.0 * cur_acc);
}

static inline double _compute_center_cordi(double ratio_m, double ratio_n,
                                           double cordi_a, double cordi_b) {
  /* (mx2 - nx1) / (m - n) */
  return ((ratio_m * cordi_b) - (ratio_n * cordi_a)) / (ratio_m - ratio_n);
}

static inline std::pair<double, double> _compute_next_cordi(double base_x,
                                                            double base_y,
                                                            double x, double y,
                                                            double theta) {
  std::pair<double, double> ret;

  /* x` = a + (x - a)cos(theta) - (y - b)sin(theta) */
  ret.first = base_x + ((x - base_x) * cos(theta) - (base_y - y) * sin(theta));

  /* y` = b + (x - a)sin(theta) + (y - b)cos(theta) */
  ret.second = base_y - ((x - base_x) * sin(theta) + (base_y - y) * cos(theta));

  return ret;
}

static inline QRectF _set_rect(double x, double y, double radius) {
  return QRectF(x - radius, y - radius, radius * 2.0, radius * 2.0);
}

static inline int _set_start_angle(double cx, double cy, double x, double y) {
  int ret = 0;

  if (cx == x) {
    ret = cy > y ? 90 : 270;
  } else if (cy == y) {
    ret = cx > x ? 180 : 0;
  } else {
    /* theta = atan((y2 - y1) / (x2 - x1)) */
    double result;
    double angle = atan((cy - y) / (cx - x)) * RADIAN_TO_DEGREE;

    if (cx < x && cy > y)
      result = fabs(angle);
    else if ((cx > x && cy > y) || (cx > x && cy < y))
      result = 180.0 - angle;
    else
      result = 360.0 - angle;

    /* multiple 16 for drawing */
    ret = static_cast<int>(result * 16.0 + 0.5);
  }

  return ret;
}

static inline int _set_straight_cordi(double *x1, double *y1, double *x2,
                                      double *y2, double dist) {
  if (*y2 - *y1 == 0.0) {
    if (*x1 < *x2) {
      *y1 -= dist;
      *y2 -= dist;
    } else {
      *y1 += dist;
      *y2 += dist;
    }
  } else if (*x2 - *x1 == 0.0) {
    if (*y1 < *y2) {
      *x1 += dist;
      *x2 += dist;
    } else {
      *x1 -= dist;
      *x2 -= dist;
    }
  } else {
    double angle = atan(fabs((*y2 - *y1) / (*x2 - *x1))) * RADIAN_TO_DEGREE;
    double theta = (90.0 - angle) * DEGREE_TO_RADIAN;
    double x = dist * cos(theta);
    double y = dist * sin(theta);

    if (*y1 > *y2) {
      /* 1, 2 quadrant */
      if (*x1 < *x2) {
        x *= -1.0;
        y *= -1.0;
      } else {
        x *= -1.0;
      }
    } else {
      /* 3, 4 quadrant */
      if (*x1 < *x2)
        y *= -1.0;
    }

    *x1 += x;
    *y1 += y;
    *x2 += x;
    *y2 += y;
  }

  return 0;
}
