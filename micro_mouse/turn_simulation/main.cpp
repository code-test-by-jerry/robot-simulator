/* Copyright 2022. Lee, Jerry J all rights reserved */

#include <QApplication>

#include <gui/turn_simulation.h>

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);

  MainWindow mainWindow;
  mainWindow.show();

  return app.exec();
}
