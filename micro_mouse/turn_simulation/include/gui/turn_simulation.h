/* Copyright 2022. Lee, Jerry J all rights reserved */

#ifndef TURN_SIMULATION_H
#define TURN_SIMULATION_H

#include <data/common_compute.h>
#include <data/robot_information.h>

#include <QMainWindow>
#include <QPainter>
#include <QValidator>
#include <memory>
#include <vector>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

public: // NOLINT
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow() override;

protected: // NOLINT
  void paintEvent(QPaintEvent *paintEvent);

  int DrawLines(QPainter *painter);
  int DrawEnter(QPainter *painter);
  int DrawAccel(QPainter *painter);
  int DrawConstVel(QPainter *painter);
  int DrawDecel(QPainter *painter);
  int DrawEscape(QPainter *painter);

private slots: // NOLINT
  void Clear(void);
  int Start(void);

private: // NOLINT
  int CheckRobotInfo(void);

  Ui::MainWindow *ui;
  std::unique_ptr<CommonCompute> start;
  std::unique_ptr<CommonCompute> end;
  std::unique_ptr<RobotInformation> robot;

  std::vector<std::unique_ptr<QValidator>> validator;

  bool start_flag;
};

#endif /* TURN_SIMULATION_H */
