/* Copyright 2022. Lee, Jerry J all rights reserved */

#ifndef ROBOT_INFORMATION_H
#define ROBOT_INFORMATION_H

enum class Data : int {
  kUtime = 500,
  kSpeed = 650,
  kEnter = 30,
  kAccel = 94,
  kConstVel = 244,
  kEscape = 28,
  kTread = 70,
  kLeftAcc = 7000,
  kRightAcc = 7000,
  kTotal = 9
};

class RobotInformation {
public: // NOLINT
  RobotInformation();
  virtual ~RobotInformation() = default;

  void SetUtime(int utime);
  void SetSpeed(int speed);
  void SetEnter(int enter);
  void SetAccel(int accel);
  void SetConstVel(int const_vel);
  void SetEscape(int escape);
  void SetTread(int tread);
  void SetLeftAcc(int left_acc);
  void SetRightAcc(int right_acc);
  void SetLeftVel(double left_vel);
  void SetRightVel(double right_vel);

  int GetUtime(void) const;
  int GetSpeed(void) const;
  int GetEnter(void) const;
  int GetAccel(void) const;
  int GetConstVel(void) const;
  int GetEscape(void) const;
  int GetTread(void) const;
  int GetLeftAcc(void) const;
  int GetRightAcc(void) const;
  double GetLeftVel(void) const;
  double GetRightVel(void) const;

private: // NOLINT
  int utime;
  int speed;
  int enter;
  int accel;
  int const_vel;
  int escape;
  int tread;
  int left_acc;
  int right_acc;
  double left_vel;
  double right_vel;
};

#endif /* ROBOT_INFORMATION_H */
