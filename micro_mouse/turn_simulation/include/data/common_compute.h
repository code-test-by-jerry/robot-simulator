/* Copyright 2022. Lee, Jerry J all rights reserved */

#ifndef COMMON_COMPUTE_H
#define COMMON_COMPUTE_H

#include <QRectF>
#include <utility>

enum class Coordinates : int {
  kStraightDef = 540,
  kDiagonalXDef = 540,
  kDiagonalYDef = 360,
  kAddRef = 20
};

class CommonCompute {
public: // NOLINT
  CommonCompute();
  virtual ~CommonCompute() = default;
  CommonCompute &operator=(const CommonCompute &cc);

  void SetStraightMode(const int robot_tread);
  void SetDiagonalMode(const int robot_tread);

  double GetCurLeftX(void) const;
  double GetCurRightX(void) const;
  double GetCurLeftY(void) const;
  double GetCurRightY(void) const;

  QRectF GetLeftCordi(void) const;
  QRectF GetRightCordi(void) const;

  int GetStartAngle(void) const;
  int GetSpanAngle(void) const;

  void EnterOrEscape(const int utime, const int speed);
  std::pair<double, double> Accel(const int utime, const double left_vel,
                                  const double right_vel, const int left_acc,
                                  const int right_acc, const int robot_tread,
                                  const int ref_cordi);
  int ConstVel(const int utime, const double left_vel, const double right_vel,
               const int robot_tread, const int ref_cordi);

private: // NOLINT
  void compute_moving_data(double next_left_dist, double next_right_dist,
                           double tread, double base);

  double cur_left_x;
  double cur_right_x;
  double cur_left_y;
  double cur_right_y;

  QRectF left_cordi;
  QRectF right_cordi;

  int start_angle;
  int span_angle;
};

#endif /* COMMON_COMPUTE_H */
