/* Copyright 2022. Lee, Jerry J all rights reserved */

#include <gui/turn_simulation.h>

#include <QMessageBox>
#include <QWidget>
#include <cerrno>
#include <cmath>
#include <utility>

#include "./ui_turn_simulation.h"

#define DRAW(x1, y1, x2, y2)                                                   \
  do {                                                                         \
    QLineF diagonal;                                                           \
    diagonal.setP1(QPointF(x1, y1));                                           \
    diagonal.setP2(QPointF(x2, y2));                                           \
    painter->drawLine(diagonal);                                               \
  } while (0)

#define ERROR_GUARD(condition, msg_box, err_msg)                               \
  do {                                                                         \
    if (condition == true) {                                                   \
      msg_box.setText("Need to input the " err_msg);                           \
      msg_box.exec();                                                          \
      return -EPERM;                                                           \
    }                                                                          \
  } while (0)

#define SET_ROBOT_INFO(ui_edit, set_func, err_msg)                             \
  do {                                                                         \
    str = ui_edit->text().toStdString();                                       \
    ERROR_GUARD((str.length() == 0), msg_box, err_msg);                        \
    set_func(std::stoi(str));                                                  \
  } while (0)

static inline void _draw_vertical(QPainter *painter, const CommonCompute &start,
                                  const CommonCompute &end, int base);
static inline void _draw_horizontal(QPainter *painter,
                                    const CommonCompute &start,
                                    const CommonCompute &end, int base);
static inline void _draw_sector(QPainter *painter, const CommonCompute &tart);

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow),
      start(std::make_unique<CommonCompute>()),
      end(std::make_unique<CommonCompute>()),
      robot(std::make_unique<RobotInformation>()), start_flag(true) {
  ui->setupUi(this);

  /* init the vector */
  validator.reserve(static_cast<int>(Data::kTotal));
  validator.push_back(std::make_unique<QIntValidator>(0, 1000, this));
  validator.push_back(std::make_unique<QIntValidator>(0, 2000, this));
  validator.push_back(std::make_unique<QIntValidator>(0, 500, this));
  validator.push_back(std::make_unique<QIntValidator>(0, 200, this));
  validator.push_back(std::make_unique<QIntValidator>(0, 800, this));
  validator.push_back(std::make_unique<QIntValidator>(0, 500, this));
  validator.push_back(std::make_unique<QIntValidator>(0, 100, this));
  validator.push_back(std::make_unique<QIntValidator>(0, 20000, this));
  validator.push_back(std::make_unique<QIntValidator>(0, 20000, this));

  /* Fixed the digit range */
  ui->timeEdit->setValidator(validator[0].get());
  ui->speedEdit->setValidator(validator[1].get());
  ui->enterEdit->setValidator(validator[2].get());
  ui->accelEdit->setValidator(validator[3].get());
  ui->constVelEdit->setValidator(validator[4].get());
  ui->escapeEdit->setValidator(validator[5].get());
  ui->treadEdit->setValidator(validator[6].get());
  ui->lAccEdit->setValidator(validator[7].get());
  ui->rAccEdit->setValidator(validator[8].get());

  /* connect the GUI */
  QObject::connect(ui->clear, SIGNAL(clicked()), this, SLOT(Clear()));
  QObject::connect(ui->start, SIGNAL(clicked()), this, SLOT(Start()));
}

MainWindow::~MainWindow() {
  for (auto iter = validator.begin(); iter != validator.end(); iter++)
    *iter = nullptr;

  robot = nullptr;
  end = nullptr;
  start = nullptr;

  delete ui;
}

void MainWindow::paintEvent(QPaintEvent *paintEvent) {
  Q_UNUSED(paintEvent);

  QPainter painter(this);

  /* draw base X */
  DrawLines(&painter);

  /* draw smooth turn */
  if (start_flag == false) {
    DrawEnter(&painter);
    DrawAccel(&painter);
    DrawConstVel(&painter);
    DrawDecel(&painter);
    DrawEscape(&painter);

    start_flag = true;
  }
}

int MainWindow::DrawLines(QPainter *painter) {
  /* Create the line object: */
  int x = 0, y = 0;
  const int cordi[] = {0, 180, 360, 540, 720};
  const int base = static_cast<int>(Coordinates::kAddRef);

  /* draw X */
  for (y = 0; y < 4; y++) {
    for (x = 0; x < 4; x++) {
      DRAW(base + cordi[x], base + cordi[y], base + cordi[x + 1],
           base + cordi[y + 1]);
      DRAW(base + cordi[x + 1], base + cordi[y], base + cordi[x],
           base + cordi[y + 1]);
    }
  }

  for (x = 0; x < 5; x++) {
    /* draw vertical line */
    DRAW(base + 0, base + cordi[x], base + 720, base + cordi[x]);

    /* draw horizontal line */
    DRAW(base + cordi[x], base + 0, base + cordi[x], base + 720);
  }

  return 0;
}

int MainWindow::DrawEnter(QPainter *painter) {
  const int base = static_cast<int>(Coordinates::kAddRef);

  *end = *start;
  start->EnterOrEscape(robot->GetUtime() * robot->GetEnter(),
                       robot->GetSpeed());

  /* draw vertical line */
  _draw_vertical(painter, *start, *end, base);

  /* draw horizontal line */
  _draw_horizontal(painter, *start, *end, base);

  return 0;
}

int MainWindow::DrawAccel(QPainter *painter) {
  std::pair<double, double> speed;
  const int base = static_cast<int>(Coordinates::kAddRef);

  const int utime = robot->GetUtime();
  const int tick = robot->GetAccel();
  const int left_acc = robot->GetLeftAcc();
  const int right_acc = robot->GetRightAcc();
  const int tread = robot->GetTread();

  double left_vel = static_cast<double>(robot->GetSpeed());
  double right_vel = static_cast<double>(robot->GetSpeed());

  *end = *start;
  speed = start->Accel(utime * tick, left_vel, right_vel, left_acc, right_acc,
                       tread, base);
  if (speed.first > 0.0 && speed.second > 0.0) {
    /* draw vertical line */
    _draw_vertical(painter, *start, *end, base);

    /* draw horizontal line */
    _draw_sector(painter, *start);
  }

  /* display the speed */
  ui->lSpeedEdit->setText(QString::number(speed.first));
  ui->rSpeedEdit->setText(QString::number(speed.second));

  /* set the speed */
  QMessageBox msg_box;
  msg_box.setWindowTitle("Error");
  std::string str;
  SET_ROBOT_INFO(ui->lSpeedEdit, robot->SetLeftVel, "Left Velocity!");
  SET_ROBOT_INFO(ui->rSpeedEdit, robot->SetRightVel, "Right Velocity!");

  return 0;
}

int MainWindow::DrawConstVel(QPainter *painter) {
  const int base = static_cast<int>(Coordinates::kAddRef);

  const int utime = robot->GetUtime();
  const int tick = robot->GetConstVel();
  const int tread = robot->GetTread();

  const double left_vel = robot->GetLeftVel();
  const double right_vel = robot->GetRightVel();

  *end = *start;
  start->ConstVel(utime * tick, left_vel, right_vel, tread, base);

  /* draw vertical line */
  _draw_vertical(painter, *start, *end, base);

  /* draw horizontal line */
  _draw_sector(painter, *start);

  return 0;
}

int MainWindow::DrawDecel(QPainter *painter) {
  const int base = static_cast<int>(Coordinates::kAddRef);

  const int utime = robot->GetUtime();
  const int tick = robot->GetAccel();
  const int left_acc = robot->GetLeftAcc();
  const int right_acc = robot->GetRightAcc();
  const int tread = robot->GetTread();

  double left_vel = robot->GetLeftVel();
  double right_vel = robot->GetRightVel();

  *end = *start;
  start->Accel(utime * tick, left_vel, right_vel, -left_acc, -right_acc, tread,
               base);

  /* draw vertical line */
  _draw_vertical(painter, *start, *end, base);

  /* draw horizontal line */
  _draw_sector(painter, *start);

  return 0;
}

int MainWindow::DrawEscape(QPainter *painter) {
  const int base = static_cast<int>(Coordinates::kAddRef);

  *end = *start;
  start->EnterOrEscape(robot->GetUtime() * robot->GetEscape(),
                       robot->GetSpeed());

  /* draw vertical line */
  _draw_vertical(painter, *start, *end, base);

  /* draw horizontal line */
  _draw_horizontal(painter, *start, *end, base);

  return 0;
}

void MainWindow::Clear(void) {
  ui->timeEdit->setText(QString::number(static_cast<int>(Data::kUtime)));
  ui->speedEdit->setText(QString::number(static_cast<int>(Data::kSpeed)));
  ui->enterEdit->setText(QString::number(static_cast<int>(Data::kEnter)));
  ui->accelEdit->setText(QString::number(static_cast<int>(Data::kAccel)));
  ui->constVelEdit->setText(QString::number(static_cast<int>(Data::kConstVel)));
  ui->escapeEdit->setText(QString::number(static_cast<int>(Data::kEscape)));
  ui->treadEdit->setText(QString::number(static_cast<int>(Data::kTread)));
  ui->lAccEdit->setText(QString::number(static_cast<int>(Data::kLeftAcc)));
  ui->rAccEdit->setText(QString::number(static_cast<int>(Data::kRightAcc)));
  ui->straight->setChecked(true);

  /* reset the flag */
  start_flag = true;

  /* call the paintEvent */
  update();
}

int MainWindow::Start(void) {
  int ret = CheckRobotInfo();
  if (ret < 0)
    return -EPERM;

  start_flag = false;

  if (ui->straight->isChecked()) {
    /* straight mode */
    start->SetStraightMode(robot->GetTread());
  } else {
    /* diagonal mode */
    start->SetDiagonalMode(robot->GetTread());
  }

  update();

  return 0;
}

int MainWindow::CheckRobotInfo(void) {
  QMessageBox msg_box;
  msg_box.setWindowTitle("Error");

  std::string str;
  SET_ROBOT_INFO(ui->treadEdit, robot->SetTread, "Tread!");
  SET_ROBOT_INFO(ui->timeEdit, robot->SetUtime, "Time!");
  SET_ROBOT_INFO(ui->speedEdit, robot->SetSpeed, "Speed!");
  SET_ROBOT_INFO(ui->enterEdit, robot->SetEnter, "Enter Tick!");
  SET_ROBOT_INFO(ui->accelEdit, robot->SetAccel, "Accel Tick!");
  SET_ROBOT_INFO(ui->constVelEdit, robot->SetConstVel, "Const Vel Tick!");
  SET_ROBOT_INFO(ui->escapeEdit, robot->SetEscape, "Escape Tick!");
  SET_ROBOT_INFO(ui->lAccEdit, robot->SetLeftAcc, "Left Acc!");
  SET_ROBOT_INFO(ui->rAccEdit, robot->SetRightAcc, "Right Acc!");

  return 0;
}

static inline void _draw_vertical(QPainter *painter, const CommonCompute &start,
                                  const CommonCompute &end, int base) {
  double ref = static_cast<double>(base);

  DRAW(ref + end.GetCurLeftX(), ref + end.GetCurLeftY(),
       ref + end.GetCurRightX(), ref + end.GetCurRightY());
  DRAW(ref + start.GetCurLeftX(), ref + start.GetCurLeftY(),
       ref + start.GetCurRightX(), ref + start.GetCurRightY());
}

static inline void _draw_horizontal(QPainter *painter,
                                    const CommonCompute &start,
                                    const CommonCompute &end, int base) {
  double ref = static_cast<double>(base);

  DRAW(ref + end.GetCurLeftX(), ref + end.GetCurLeftY(),
       ref + start.GetCurLeftX(), ref + start.GetCurLeftY());
  DRAW(ref + end.GetCurRightX(), ref + end.GetCurRightY(),
       ref + start.GetCurRightX(), ref + start.GetCurRightY());
}

static inline void _draw_sector(QPainter *painter, const CommonCompute &start) {
  painter->drawArc(start.GetLeftCordi(), start.GetStartAngle(),
                   start.GetSpanAngle());
  painter->drawArc(start.GetRightCordi(), start.GetStartAngle(),
                   start.GetSpanAngle());
}
