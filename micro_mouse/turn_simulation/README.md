# Micro Mouse Turn Simulation

  * Using the Qt 5.9.5, QMake 3.1

```bash
$ qmake --version
QMake version 3.1
Using Qt version 5.9.5 in /usr/lib/x86_64-linux-gnu
```


## How to build the tool

```bash
$ mkdir -p out
$ cd out && cmake ../
$ make
```


## How to execute the tool

```bash
$ cd out
$ ./turn_simul
```
